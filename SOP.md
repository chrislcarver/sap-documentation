# Purpose

Achieving maintenance excellence requires dedication and commitment.  To ensure the validity and completeness of information in alignment with our strategic goals and expectations of GLOBALFOUNDRIES, Fab8 Facilities uses SAP to track, plan, and schedule maintenance. The expected outcome is to produce process results in alignment with the best practices of the business in compliance with internal and external regulatory requirements. This shall be the system of record for all recordable work and the master data source for any asset reporting or metric calculations.
	
By making this statement, we are committing to a proactive, planned, and consistent way of doing business. The purpose of this document is to increase the safety, reliability, and reduce the cost of operation for the many assets owned by the facilities organization. This document is not a procedure but a guide that explains internal business processes that shall be followed to produce undeviating and reliable outcomes. This document will make references to technical SOP’s and specifications when needed.

# Scope

This document applies to the Fab8 Facilities organization, specifically the Facilities Engineering Department tasked with the responsibility for maintaining assets which support Fab Operations. The document defines the required support roles and responsibilities needed to ensure the continued effort in achieving maintenance excellence as well as the business processes that shall be followed and who shall enforce the adherence of said processes.
	
The processes contained within this document speaks to include the maintenance cycle in its entirety, material procurement, daily operational tasks, and the administrative roles required by SAP.

# Roles and Responsibilities

This section outlines key activities that must be undertaken in order for Facilities to achieve maintenance excellence. While this document segregates individual roles and defines their activities and responsibilities, it does not dictate how the roles are delegated nor shall it define how an organization unit should be structured. The following is a highly recommended set of duties that should be considered to maximize productivity.

## Maintenance Manager

The maintenance manager is responsible for the direction of the maintenance excellence program. The duties of the maintenance manager shall entail:

- Direct program governance pertaining to maintenance program
- Establish vision statement
- Program sponsor for any improvement projects related to maintenance excellence
- Establishes and reviews KPIs
- Lead weekly planning and scheduling meetings
- Reports directly to the Facilities Director
- Maintain and review the maintenance appraisal program
- Provides monthly, quarterly, and annual reports on program health

## Front Line Manager Duties

The manager is responsible for ensuring the execution of the overall business processes defined within this document and deficiencies reported appropriately. The duties of the manager shall entail:

- Review of work order backlog
- Execute the planned schedule
- Prioritize workloads on a daily basis
- Ensure confirmations are entered accurately and timely
- Ensure scheduled work is accomplished on time
- Engage in cross work center communication for complicated work orders
- Provide feedback to system owners
- Provide feedback to the SAP facilities excellence team
- Approve direct order requests
- Review and understand metrics
- Review completed maintenance items for completeness and accuracy
- Complete SAP order life cycle when maintenance items are finished
- Complete and review maintenance appraisals

## System Owner Duties

The system owner is charged with the overall operational health of the assets assigned to them. The system owner shall define, implement, and work with management to enforce the timely completion of all maintenance related to the assets in their care. The duties of the system owner shall include the following:

- Review the backlog for their systems.
- Create and manage notifications for their systems.
- Create all necessary procedures for all work.
- Communicate to create bill of material assemblies for equipment.
- Ensure all equipment is modeled correctly within SAP.
- Ensure all equipment catalog profiles are maintained and accurate.
- Ensure all maintenance is current and all SOPs are attached to equipment.
- Review all preventative maintenance is accomplished according to schedule.
- Own the approval process for waving preventative maintenance items.
- Review all completed maintenance items related to owned systems.
- Communicate with management on scheduling work orders.

## Planner/Scheduler Duties

The planner/scheduler has the responsibility to ensure work is prepared, ready to work, and resources available before it is put on the job list. This person should be a senior individual who understands the nature of corrective maintenance and has the ability to communicate required steps adequately through a written procedure or structured operations. Ideally this individual would be removed from maintenance duties while carrying out this role. The following list of duties shall be executed by the planner/scheduler:

- Create the work schedule to be executed by their respective work center
- Communicate with system owners and manager to work the backlog.
- Communicate with technicians / manager on current work progress.
- Work with system owners on procedures for corrective maintenance items.
- Work with system owners to upload any approved procedures to the work order 
- Work with system owners to upload any approved procedures  to equipment within SAP.
- Work with system owners and manager to procure materials to accomplish maintenance.
- Interact with SAP to update notifications and work orders.
- Process notifications
- Review all work completed for their work center.
- Review new notifications and work orders to ensure compliance. 
- Report any compliance deviations to management.

## Technician Duties

This individual is responsible to carry out the instructions on the work order following a specific procedure. The technician shall have no other responsibility other than to execute the work order and report all activities in SAP in a timely fashion. The technician will:

- Work with their manager to understand the work for the day.
- Communicate with planner / manager any delays or issues with work orders.
- Provide feedback to planner / system owners / manager on procedural issues.
- Enter all time confirmations before end of shift. 
- Provide descriptive comments.
- Enter new corrective maintenance items if deficiencies are found

## SAP Administrator Duties

An SAP administrator within the Facilities organization is responsible to maintain the system and drive continuous process improvements as well as other administrative duties. The duties shall include:

- Support the business in maintaining technical objects
- Support the business in maintaining maintenance plans
- Support the business in maintaining BOM’s 
- Support the business in maintaining vital master data.
- Work with Manufacturing Technologies (MT) to deliver improvements via release cycles.
- Work with users to understand new requirements and effectively communicate solutions.
- Ensure metrics are reported and understood.
- Develop and deliver training to SAP users.
- Develop and maintain SAP procedures.


# Definitions
## Imperatives {.definition}

The keywords "MUST", "MUST NOT", "REQUIRED", "SHALL", "SHALL NOT", "SHOULD", “SHOULD NOT", "RECOMMENDED",  "MAY", and "OPTIONAL" in this document are to be interpreted as described below:

1. MUST – This word, or the terms "REQUIRED" or "SHALL", mean that the definition is an absolute requirement of the specification.
2. MUST NOT – This phrase, or the phrase "SHALL NOT", mean that the definition is an absolute prohibition of the specification.
3. SHOULD – This word, or the adjective "RECOMMENDED", mean that there may exist valid reasons in particular circumstances to ignore a particular item, but the full implications must be understood and carefully weighed before choosing a different course.
4. SHOULD NOT – This phrase, or the phrase "NOT RECOMMENDED" mean that there may exist valid reasons in particular circumstances when the particular behavior is acceptable or even useful, but the full implications should be understood and the case carefully weighed   before implementing any behavior described with this label.
5. MAY – This word, or the adjective "OPTIONAL", means that an item is truly optional. This may be used whenever it best suits the business. It will not affect defined KPIs or Metrics, or any other required business process.

## ABC Indicator {.definition}
<a name="abc"></a>

Represents the risk incurred if the asset is not functional. The value A being  the highest risk, and the value F being the lowest. More detail is provided within this [section](#abc-spec).

## Acquisition Date {.definition}
<a name="acquisition-date"></a>

Date when the asset was purchased.

## Acquisition Value {.definition}
<a name="acquisition-value"></a>

Total cost the asset was purchased for. This combines with the currency type that the purchase was made in.

## Authorization Group {.definition}
<a name="authorization-group"></a>

This is a security key that is required on all equipment. The key will allow specific user groups the ability to interact with that object. This is intended to give vendors access to only the information they are required to view. More details on the specific types of security keys available are found in this section.

## Backlog {.definition}
<a name="backlog"></a>

The backlog shows how much future work is outstanding and is measured in weeks of work. 

## Bill of Material (BOM) {.definition}
<a name="bom"></a>

The bill of material is a curated list of parts that belong to the asset. 

## Catalog Profile {.definition}
<a name="catalog-profile"></a>

The catalog profile defines the set of codings used for failure analysis. These codings include symptom, object, damage, and cause codes. Each equipment is assigned a profile which can be unique to that asset or shared among like assets.

## Corrective Maintenance {.definition}
Corrective maintenance is an identified deficiency of a piece of equipment which requires action to correct. All work that is the result of an identified deficiency SHALL be documented as corrective maintenance. All corrective work SHALL have an approved procedure in the form of a PTP, SIPP, or approved steps on the work order.

## Cost Center {.definition}
<a name="cost-center"></a>

The financial account to which any material orders will be charged to.

## Description {.definition}
<a name="description"></a>

English textual description of the item that is being added to the system

## Equipment {.definition}
<a name="equip-definition"></a>

Equipment is defined in full in the [equipment](#equipment-scope) section

## Maintenance Plant {.definition}
<a name="maintenance-plant"></a>

Represents an organization or plant.

## Notification {.definition}
The notification is the foundation upon which a work order is built. It is an object within SAP that contains several important pieces of information. Important aspects of a notification include:

- All work orders have notifications.
- All comments that are made throughout the life cycle of a work order are stored to the corresponding notification.
- The notification contains the cause, damage, and failure codes which can be used at a later time for failure analysis.
- The notification acts as a backlog for corrective work.

## Object Type {.definition}
<a name="object-type"></a>

A high level organizational category which helps group like equipment. This will help searching for like equipment easier in SAP. If your type is not found here, please reach out so we can get that added for you.

## Operation {.definition}
<a name="operation"></a>

An operation is a step in a work order. It may be a simple directive to follow an SOP to accomplish a given preventative maintenance item, or it may be the performance of a specific function like a lock out tag out. A work order can have one or many operations and operations may be defined during the creation of the work order or pulled from a task list. An operation has its own system and user status which can help track the overall progress of the work order. An operation also has a corresponding work center and operation level equipment that can differ from the main work center and equipment associated with the order. All work orders will have at least one operation.

## Planner Group {.definition}
<a name="planner-group"></a>

A group responsible for the planning and processing of maintenance tasks in a particular plant.

<a name="preventative-maintenance"></a>

## Preventative Maintenance {.definition}

Preventative maintenance is the planned activities required by either the manufacturer or the system engineer to a particular asset or equipment to maintain its reliability or safety. Preventative maintenance is scheduled either on a time based (cyclic) rotation or conditioned based. Within SAP, all maintenance work orders are generated from a maintenance plan which defines all of the maintenance attributes. All preventative maintenance items shall have an approved procedure.

<a name="projects"></a>

## Projects {.definition}

Work activities which are neither corrective in nature nor preventative, yet require resources which affect an asset can be classified as project work. Project work SHOULD be initiated by a system owner or a manager and SHALL be planned with the other work in the backlog. Project work SHALL have an approved PTP, SIPP, approved procedure, or approved steps as operations on the work order.

<a name="system-status"></a>

## System Status {.definition}

The system status is a set of flags which represent the different states of a notification and/or work order. This status is set by the system and only changes when specific actions are taken by the user. Each order type has a unique set of rules that dictate how system statuses are applied to the notification or order. Each object may have many system statuses at the same time. A list of all system statuses are available [here](#appendix-system-status).

> A closed work order  will have the following status: **CLSD CNF  JIPR NMAT PRC  SETC**. A work order that is open and ready to work will have the following status: **REL NMAT PRC**

## Technical Objects {.definition}
<a name="technical-objects"></a>

### Functional Location {.sub-definition}
<a name="functional-location"></a>

Functional locations represent a system and/or structure in a parent/child relationship. Work may be performed against a functional location. Most standalone objects would not be modeled as functional locations. Equipment is attached to functional locations, but functional locations cannot be attached to equipment. Functional locations are rigid in nature, meaning during the life cycle of the asset it should not change.

> Locations i.e. Fab A/B/C and systems i.e. an entire chemical or gas distribution system from point of material connection to fab tool point of connection.

### Equipment {.sub-definition}
<a name="equipment"></a>

Equipment in SAP represents an object. Each piece of equipment is unique and can be ‘installed’ into the equipment hierarchy within SAP. Equipment can be moved from one place to another within SAP, while still retaining the history of the asset. Equipment may have many attributes which, should be assigned in order to apply unique features or to track special attributes.  Equipment is attached to functional locations and may also have equipment attached to it, known as sub-equipment. It may exist all by itself, independent of a functional location or superior equipment.

> Equipment may be something as small as a chair, or as large as an acid scrubber.

## Time Confirmation
<a name="time-confirmation"></a>

A time confirmation is an entry within SAP representing how much [wrench time](#wrench-time) a user spent on a specific operation. A user can make a normal confirmation or a final confirmation signaling that the operation is complete.

## User Status {.definition}
<a name="user-status"></a>

User statuses are similar to system status, but can be changed by the user. User Status shows where a work order is in the process. User statuses may be “keyed”, meaning only one can be selected, while “non-keyed” status allow for multiple selections to be made. A list of all user statuses are available [here](#appendix-user-status).

> A monthly PM that was completed will have the following status: **CMPL**. A corrective maintenance order that has not been completed that is on hold will have the following status:  **HOLD HFAB HMAT**

## Work {.definition}
<a name="work"></a>

Maintenance activities involve employees, usually internal, performing work which involves repairing, improving, altering, or planned actions on an asset. Any work which directly correlates to an asset shall be documented in SAP. Vendors who perform maintenance shall also be required to document work either directly or by proxy.

## Work Center {.definition}
<a name="work-center"></a>

A group or organization representing a craft or skill set. Work centers are comprised of individuals in SAP's HR mini master. Work centers are assigned to notifications, work orders, operations within task lists, maintenance plans, and other master data items.

## Work Order {.definition}
<a name="work-order"></a>

A work order is created from a notification. It is its own object within SAP. A work order is the main system interface to record maintenance and operational activities performed in relation to facilities. Some important aspects to a work order are:
-	Time confirmations are made to the work order.
- 	Parts are ordered against a work order.
-	Task lists are found within the work order.

## Wrench Time {.definition}
<a name="wrench-time"></a>

Wrench time is work spent directly performing explicitly defined procedural steps required to be executed as documented within the SAP task list or within the SOP referenced on the task list.