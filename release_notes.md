# Release Notes

- [10.8](#10.8)
- [10.7](#10.7)
- [10.6](#10.6)

<a name="10.8"></a>

## 10.8

Domain MSR | System MSR | Description | Status | State
-----------|------------|-------------|--------|------
MSR1266941|MSR1270174|Incorrect nomenclature displayed in Notification Processing application|Approved|Dev Complete
MSR1092515|MSR1094447|Required the ability to mass edit the partner associated with work orders.|Approved|Dev Complete
MSR1303900|MSR1308548|2017P: SAP PM For Fab 9/10 Facilities; Step down of IBM Maximo and the IBM SDA to GF solution for Fab 9 and 10 Facilities maintenance system|Approved|Dev Complete
MSR1303900|MSR1308629|SAP PM Security roles for Fab9 and Fab10|Approved|Dev Complete
MSR1304423|MSR1308553|Coding that isn't applicable to 8F02 and 8F03 work orders is visible in Work Order Execution|Approved|Dev Complete
MSR1310456|MSR1312833|Change Daily PM Report to include compliance end date.|Approved|Dev Complete
MSR1151489|MSR1277946|Implement resist batch qualification to support Fab 8 MCM & APC requirements (10.8)|Approved|Dev Complete
MSR1295880|MSR1308405|MCM To add in the report capability to MCM for planning materials|Approved|Dev Complete
MSR1292840|MSR1308483|Gas part number exclusions from MCM Expiry Alerts|Approved|Dev Complete
MSR1284130|MSR1308635|MCM Addition Planning Status needed in MCM Planning|Approved|Dev Complete
MSR1284216|MSR1308570|MCM Show Sort field for Facility Equipment for Second Technician Scanning|Approved|Dev Complete
MSR1256001|MSR1308573|Remove(Hide) Material from Work Center Material List|Approved|Dev Complete
MSR1229850|MSR1309988|Additional MCM roles required for water group & mechanical group.|Approved|Dev Complete

<a name="10.7"></a>

## 10.7

In the process of updating

<a name="10.6"></a>

## 10.6

In the process of updating
