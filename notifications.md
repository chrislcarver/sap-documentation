# Notifications
<!--
<ul class="navigation navigation-main navigation-accordion">
<li><a href="#introduction">Introduction</a></li>
<li><a href="#notification-types">Notification Types</a></li>
<li><a href="#creating">Creating (IW21)</a></li>
<li><a href="#tabs">Tabs</a>
<ul>
<li><a href="#notification-tab">Notification</a></li>
<li><a href="#malfunction-tab">Malfunction, Breakdown</a></li>
<li><a href="#location-tab">Location Data</a></li>
<li><a href="#scheduling-tab">Scheduling Overview</a></li>
<li><a href="#items-tab">Items</a></li>
<li><a href="#tasks-tab">Tasks</a></li>
<li><a href="#activities-tab">Activities</a></li>
<li><a href="#order-tab">Superior Order</a></li>
</ul></li>
<li><a href="#searching">Searching (IW28)</a></li>
<li><a href="#editing">Editing (IW22)</a></li>
<li><a href="#statuses">Statuses</a>
<ul>
<li><a href="#system">System</a></li>
<li><a href="#user">User</a></li>
</ul></li>
<li><a href="#processing">Processing</a></li>
</ul>
-->
# Notifications
<a name="introduction"></a>
##Introduction

A notification is the first step in the life cycle of a maintenance request in SAP. It represents the foundation for the work order that will be generated later in the process. A notification holds many different data attributes such as the failure analysis, breakdown dates, what assist it is assigned to, which work center is responsible for the work and what planner group should be responsible for planning this activity.

The notification also holds all the commentary that is produced during the life of the both the notification and the work order. Any comments made on a work order is stored to the notification.

<a name="notification-types"></a>
##Notification Types

Through configuration, each notification can be configured to require different data attributes, display different tabs, or contain different statuses. It is up to the business to model its process to define different notification types. The following are the notification types defined for the Malta Facilities site:

|Type|Description|
|----|----|
|81|Corrective|
|82|Preventative|
|83|Project Work|
|84|Pass Down|
|85|ERT|
|86|Operations|
|87|Chemical On/Off Loads|

<a name="creating"></a>
##Creating Notifications (IW21)

To start, enter the transaction code IW21. You will be presented with a screen that asks for the notification type, the notification number, and the reference notification number. The only required field you need to fill in is the notification type.

<nav class="level">
    <div class="level-item">
        <img src="/images/notifications/create-pm-notification-initial.jpg">
    </div>
</nav>

If you are making similar notifications (maybe for like equipment), you can reference a notification which SAP will copy into the new one allowing you to only enter minor detail changes saving the time of recreating the entire document.

<a name="tabs"></a>
##Notification Tabs

After providing which notification type you would like to create, you will be taken to the notification creation screen. The blow screen captures may be different to what you see. This is because each notification type is configured differently. This means you may have different tabs present then what will be documented here. For this documentation, we will assume the creation of an 81 type notification with the current configuration set within the Malta Facilities production instance.

<a name="notification-tab"></a>
###Notification

This tab contains details on the asset being assigned to the notification, details on why the notification is being generated, who owns responsibility for it, when the business would like to start and finish the notification, priority, and a quick failure analysis view. The only **required** field is the description input field. It is good practice to enter as much detail as possible for all available fields. Below is a screen capture of what a potentially filled out notification tab would look like:

<nav class="level">
    <div class="level-item">
        <img src="/images/notifications/create-notification-notification-tab.jpg">
    </div>
</nav>

<a name="malfunction-tab"></a>
###Malfunction, Breakdown

This tab contains details about the when this event occurred and what impact it has on the assets ability to perform its intended function. The malfunction start date and time represents when this event started, not when the issue was found. Sometimes this information is not available at which point a best case determination may be used. Malfunction end date and time represents when the asset was returned to normal operating condition, not when the work was finished.

The breakdown checkbox flags this asset as unavailable for production. An example would be if you find a slow water drip from a valve on an asset. You would still document the work, but the asset can continue to run and perform its function. If, on the other hand, it was leaking fuel oil, you may have to shut down the asset, and at this point, the breakdown flag would be checked.

During the creation process, the malfunction end date and time and the breakdown duration would be blank.

<nav class="level">
    <div class="level-item">
        <img src="/images/notifications/create-pm-notification-malfunction.jpg">
    </div>
</nav>

<a name="location-tab"></a>
###Location Data

The location tab copies information from the asset you assigned in the notification tab. You may edit the copied data here, however there is currently no use case to do so.

<nav class="level">
    <div class="level-item">
        <img src="/images/notifications/location-tab.jpg">
    </div>
</nav>

<a name="scheduling-tab"></a>
###Scheduling Overview

This tab has some duplicated fields from other tabs. You may change the person who is reporting the notification, the desired start and end dates for the notificaion as well as the malfunction start and end dates.

<nav class="level">
    <div class="level-item">
        <img src="/images/notifications/scheduling-tab.jpg">
    </div>
</nav>

<a name="items-tab"></a>
###Items

The items tab shows a list of object failure codes in a spreadsheet format. The object failure code represents what component on the asset has failed. Next to the object code is the damage code. This represents the classification of damage that was found on the object. For this example, we have the object part listed as foreline and the corresponding  damage was device not responding. You may add additional objects to this list with different damage codes. Building a robust failure catalog will go a long way in root cause analysis as well as future data mining for previous issues. Within this tab, there are three other tabs related to the items failure analysis. This means for each row you enter here may have multiple causes, tasks, and/or activities. 

<nav class="level">
    <div class="level-item">
        <img src="/images/notifications/items-items-tab.jpg">
    </div>
</nav>

####Causes
This section allows you to select what caused the failure of the selected item. You will notice at the top of this tab, you will see which item you are working against. In this case, we are working on the first line item (Device not responding). As you can see, you may enter multiple causes for this one object and damage code.

<nav class="level">
    <div class="level-item">
        <img src="/images/notifications/items-causes-tab.jpg">
    </div>
</nav>

####Tasks
This section allows you to define specific tasks that need to be performed related to this damage. This is a perfect place to record follow up actions that need to happen outside of the actual corrective work that needs to take place.


<article class="message is-info">
<section class="message-body">
<div class="media">
    <div class="media-left">
        <span class="icon is-large is-info">
        <i class="material-icons md-48">info_outline</i></span>
    </div>
    <div class="media-content">
        Adding tasks will change the system status to include OSTS (Outstanding Tasks). This is also a way to search for notifications with outstanding tasks quickly.
    </div>
</div>
</section>
</article>
<article class="message is-info">
<section class="message-body">
<div class="media">
    <div class="media-left">
        <span class="icon is-large is-info">
        <i class="material-icons md-48">warning</i></span>
    </div>
    <div class="media-content">
        You will not be able to close out the notification until all tasks are marked as complete. We currently do not fully utilize this feature and various other aspects are not configured for use.
    </div>
</div>
</section>
</article>

<nav class="level">
    <div class="level-item">
        <img src="/images/notifications/items-tasks-tab.jpg">
    </div>
</nav>

####Activities
Activities are just that, they are the codes that define the actions taken to correct the deficiency noted on the object part. Again, you may enter multiple activities that were performed on this notification. In this example, we calibrated the controller. 

<nav class="level">
    <div class="level-item">
        <img src="/images/notifications/items-activities-tab.jpg">
    </div>
</nav>

<a name="tasks-tab"></a>
###Tasks

This is exactly like the tasks within the <a href="#items-tab">Items Tab</a> with the exception that this task is not tied to a failure code. This allows for tasks at the notification level and tasks at the failure analysis level. This degree of granularity is usually disregarded in organizations that have yet to achieve maintenance excellence. The same warnings exist for these tasks as the tasks assigned at the failure coding level.

<nav class="level">
    <div class="level-item">
        <img src="/images/notifications/tasks-tab.jpg">
    </div>
</nav>

<a name="order-tab"></a>
###Superior Order

If this notification is a result of a parent order generating a child order, the parent order will be displayed here.

<nav class="level">
    <div class="level-item">
        <img src="/images/notifications/order-tab.jpg">
    </div>
</nav>

<a name="searching"></a>
##Searching (IW28 / IW29)

To search for notifications, you can use two different transactions. IW28 is to search and edit the notifications that are returned. IW29 is the same but is read only. Generally it is a good practice to search via read only so you do not lock out notifications when you open them up. By default, the search page for notifications contains many fields, most of which are not being used currently in our system. It is encouraged to create custom views of these transactions to suit your needs. To find out how to create variants (saved searches), head over to the variant documentation.

<a name="editing"></a>
##Editing (IW22)

If you know the notification number, you can use IW22 to directly open the notification and edit it without having to search for it. The read only version of this transaction is IW23.

<a name="statuses"></a>
##Statuses

SAP makes heavy use of statuses to reflect the different stages an order, object, asset, or other model is in. There are two different types of statuses: System, and User.

<a name="system"></a>
##System Statuses

The system status is completely derived from internal processes. This means that the user does not have direct control over which status is applied. Instead the system changes the status as the user interacts with the object. This is all derived through configuration and each notification type may have its own set of system statuses. In Malta, the process for 81, 82, and 83 type notifications are similar and the system statuses are shown below:

|Status|Description|
|----|----|
|OSNO|Outstanding Notification|
|OSTS|Outstanding Task|
|ORAS|Order Assigned|
|NOCO|Notification Completed|
|NOPR|Notification In Process|
|DLFL|Deletion Flag|

References: Notification Spec | Notification Business Process

<a name="user"></a>
##User Statuses

The user status is a combination of system processes and direct manipulation by the user. Configuration can trigger the setting of specific user statuses based on system interaction by the end user. It can also be open to let the user select the specific status they choose. This is beneficial allowing further segregation of notifications into various processes inside a larger process. There are also two different types of user statuses: Keyed and non keyed. Keyed statuses are only allowed to have one of them selected while non keyed can have multiple selected at once.

|Status|Type|Description|
|----|----|----|
|NAPP|Keyed|Notification Approved|
|NAWA|Keyed|New and Awaiting Approval|
|CANC|Keyed|Canceled|
|DUPE|Non Keyed|Duplicate|
|HAPM|Non Keyed|Has an associated PM|
|INIC|Non Keyed|Incorrect Information|
|MDNN|Non Keyed|Manager Deemed Not Necessary|
|SODN|Non Keyed|System Owner Deemed Not Necessary|
|WNTP|Non Keyed|Wrong Notification Type|

To change a user status for a notification, click the green check mark with the pencil button located at the top right under the notification number.

<nav class="level">
    <div class="level-item">
        <img src="/images/notifications/status-change.gif">
    </div>
</nav>

<article class="message is-info">
<section class="message-body">
<div class="media">
    <div class="media-left">
        <span class="icon is-large is-info">
        <i class="material-icons md-48">info_outline</i></span>
    </div>
    <div class="media-content">
        It is important to note that adjusting a user status does not trigger a system response. For example setting the user status to CNCL (Canceled) will not automatically cancel and close the notification. There are further steps that are needed to be executed in order to properly disposition the notification.
    </div>
</div>
</section>
</article>

References: Notification Spec | Notification Business Process

<a name="processing"></a>
##Processing

A work order should only be created if it is with high certainty that work will commence within 30 days. Creating a work order to create a work order clutters the system and serves no purpose. In order to process a notification to a work order it is as simple as one button. First, find the notification you wish to process, then click on the create order button.

<nav class="level">
    <div class="level-item">
        <img src="/images/notifications/process.jpg">
    </div>
</nav>

The next screen will ask what type of order you want to create from this notification. SAP allows you to create a different order type than what was defined on the notification. For example, if someone submits a corrective request, but it's actually a project type work order, you can change that here. You may also change the work center that will be copied to the work order. Once the order type is selected, it cannot be changed. The work center may be changed later in the work order.

<nav class="level">
    <div class="level-item">
        <img src="/images/notifications/process-2.jpg">
    </div>
</nav>

The last screen is only shown if there is a history of orders to the asset assigned on the notification. It will show the last few orders, or if there is one, some details about it. This will allow you to determine if this is a duplicate request before making a work order for it.

<nav class="level">
    <div class="level-item">
        <img src="/images/notifications/process-3.jpg">
    </div>
</nav>

Please see the documentation for <a href="/docs/master/work_orders">creating a work order</a> to continue.