# Equipment
## Scope
<a name="equipment-scope"></a>

All facilities equipment shall be modeled in SAP. Subcomponents of a parent equipment or smaller portions of a larger system may be represented in SAP depending the on business requirements. All equipment which requires periodic maintenance shall be in SAP regardless of size or significance and the maintenance plan configured in accordance with this specification. The equipment naming scheme shall follow the GLOBALFOUNDRIES Tag ID structure as much reasonably possible. See OS-XXXXX for naming convention guidance. This section outlines the definitions and requirements for all equipment entered into SAP.

## Required Fields
<a name="equipment-required-fields"></a>

The minimum data required for equipment are as follows:

- [ABC Indicator](#abc)
- [Authorization Group](#authorization-group)
- [Catalog Profile](#catalog-profile)
- [Cost Center](#cost-center)
- [Description](#description)
- [Maintenance Plant](#maintenance-plant)
- [Object Type](#object-type)
- [Planner Group](#planner-group)
- [Plant Section](#planner-group)
- [Sort Field (Tag ID)](#sort-field)
- Superior [Equipment](#equipment) or [Functional Location](#functional-location)
- [Work Center](#work-center)

## Optional Fields
<a name="equipment-optional-fields"></a>

Optional fields to further gather information are available and are listen below:

- [Acquisition Date](#acquisition-date)
- [Acquisition Value](#acquisition-value)
- [BOM](#bom)
- [Country of manufacture](#country-manufacture)
- [DMS Entry's](#dms)
- [Inventory Number](#inventory)
- [Manufacture Part Number](#oem-part)
- [Manufacture Serial Number](#oem-serial)
- [Manufacturer](#manufacturer)
- [Model Number](#model)
- [Month of construction](#construction-month)
- [Responsibility](#responsibility)
- [Responsible](#responsible)
- [Room](#room)
- [Size / Dimensions](#size)
- [Start Up Date](#start-up)
- [Warranty End](#warranty-end)
- [Warranty Start](#warranty-start)
- [Weight](#weight)
- [Year of construction](#construction-year)